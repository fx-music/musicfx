package musicFx;

import javafx.application.Application;
import javafx.stage.Stage;

import static musicFx.entry.EntryLayout.mainScene;


public class MusicApp extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        mainScene(primaryStage);
        primaryStage.show();
    }
}
