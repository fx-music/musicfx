package musicFx.layout;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import java.util.logging.Logger;

import static musicFx.entry.EntryLayout.*;


public class MainMusicController {
    private static final Logger LOGGER = Logger.getLogger("MainMusicController");
    public AnchorPane mainPane;

    public void searchClick(MouseEvent mouseEvent) {
        LOGGER.info("searchClick");
        loadComponent(SEARCH_ANCHORPANE, mainPane);
    }

    public void findMusicClick(MouseEvent mouseEvent) {
        LOGGER.info("findMusicClick");
        loadComponent(FIND_MUSIC_ANCHORPANE, mainPane);
    }

    public void mvClick(MouseEvent mouseEvent) {
        LOGGER.info("mvClick");
        loadComponent(MV_ANCHORPANE, mainPane);
    }

    public void friendsClick(MouseEvent mouseEvent) {
        LOGGER.info("friendsClick");
        loadComponent(USERS_ANCHORPANE, mainPane);
    }

    public void localMusicClick(MouseEvent mouseEvent) {
        LOGGER.info("localMusicClick");
        loadComponent(LOCAL_MUSIC_ANCHORPANE, mainPane);
    }

    public void downloadManageClick(MouseEvent mouseEvent) {
        LOGGER.info("downloadManageClick");
        loadComponent(DOWNLOAD_MANAGEMENT_ANCHORPANE, mainPane);
    }

    public void recentlyPlayClick(MouseEvent mouseEvent) {
        LOGGER.info("recentlyPlayClick");
        loadComponent(RECENTLY_PLAY_ANCHORPANE, mainPane);
    }

    public void myFavoriteClick(MouseEvent mouseEvent) {
        LOGGER.info("myFavoriteClick");
        loadComponent(MY_FAVORITE_ANCHORPANE, mainPane);
    }
}
