package musicFx.entry;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class EntryLayout {
    private static final URL main_fxml = EntryLayout.class.getClassLoader().getResource("music/main.fxml");
    public static final URL SEARCH_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/SearchAnchorPane.fxml");
    public static final URL FIND_MUSIC_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/findMusicAnchorPane.fxml");
    public static final URL MV_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/mvAnchorPane.fxml");
    public static final URL USERS_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/usersAnchorPane.fxml");
    public static final URL LOCAL_MUSIC_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/localMusicAnchorPane.fxml");
    public static final URL DOWNLOAD_MANAGEMENT_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/downloadManagementAnchorPane.fxml");
    public static final URL RECENTLY_PLAY_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/recentlyPlayAnchorPane.fxml");
    public static final URL MY_FAVORITE_ANCHORPANE = EntryLayout.class.getClassLoader().getResource("music/myfavoriteAnchorPane.fxml");
    private static final URL app_icon = EntryLayout.class.getClassLoader().getResource("music/assets/music.png");
    private static final List<String> staticNodeIdList = Arrays.asList("leftMenu", "buttom");

    public static void mainScene(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(main_fxml));
        Scene mainScene = new Scene(root);
        stage.setScene(mainScene);
        stage.setTitle("我的音乐 MUSIC");
    }

    public static void loadComponent(URL targetSource,AnchorPane mainPane) {
        try {
            Parent component = FXMLLoader.load(Objects.requireNonNull(targetSource));
            AnchorPane.setLeftAnchor(component, 150d);
            AnchorPane.setRightAnchor(component, 0d);
            AnchorPane.setTopAnchor(component, 0d);
            AnchorPane.setBottomAnchor(component, 60d);
            //排除静态元素
            mainPane.getChildren().removeIf(node -> !staticNodeIdList.contains(node.getId()));
            mainPane.getChildren().add(component);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
