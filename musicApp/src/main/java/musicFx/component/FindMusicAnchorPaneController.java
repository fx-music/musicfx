package musicFx.component;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import musicFx.basic.FXCarousel;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class FindMusicAnchorPaneController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(FindMusicAnchorPaneController.class.getName());
    public AnchorPane findMusicBasicPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOGGER.info(getClass().getName() + "initialization");
        ScrollPane scrollPane = new ScrollPane();
        AnchorPane.setRightAnchor(scrollPane, 0d);
        AnchorPane.setLeftAnchor(scrollPane, 0d);
        AnchorPane.setBottomAnchor(scrollPane, 0d);
        AnchorPane.setTopAnchor(scrollPane, 0d);
        FXCarousel carousel = new FXCarousel();
        carousel.setItems(createItems());
        carousel.setArrows(true);
        carousel.setPrefWidth(400);
        carousel.setAutoRide(true);
        carousel.setTransitionDuration(Duration.seconds(5));

        scrollPane.setContent(carousel);
        findMusicBasicPane.getChildren().add(scrollPane);
    }

    private ObservableList<Node> createItems() {

        Label lb1 = new Label("First");
        Label lb2 = new Label("Second");
        Label lb3 = new Label("Third");
        Label lb4 = new Label("Fourth");
        Label lb5 = new Label("Fifth");

        lb1.setStyle("-fx-text-fill : white;");
        lb2.setStyle("-fx-text-fill : white;");
        lb3.setStyle("-fx-text-fill : white;");
        lb4.setStyle("-fx-text-fill : white;");
        lb5.setStyle("-fx-text-fill : white;");

        VBox v1 = new VBox(lb1);
        VBox v2 = new VBox(lb2);
        VBox v3 = new VBox(lb3);
        VBox v4 = new VBox(lb4);
        VBox v5 = new VBox(lb5);

        v1.setAlignment(Pos.CENTER);
        v2.setAlignment(Pos.CENTER);
        v3.setAlignment(Pos.CENTER);
        v4.setAlignment(Pos.CENTER);
        v5.setAlignment(Pos.CENTER);

        v1.setStyle("-fx-background-color : #FF3547;");
        v2.setStyle("-fx-background-color : #512DA8;");
        v3.setStyle("-fx-background-color : #48CFAD;");
        v4.setStyle("-fx-background-color : #02C852;");
        v5.setStyle("-fx-background-color : #EC407A;");

        return FXCollections.observableArrayList(v1, v2, v3, v4, v5);
    }
}
