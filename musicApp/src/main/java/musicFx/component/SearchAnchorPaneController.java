package musicFx.component;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class SearchAnchorPaneController implements Initializable {
    private static final Logger LOGGER = Logger.getLogger(SearchAnchorPaneController.class.getName());

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        LOGGER.info(getClass().getName() + "initialization");
    }
}
