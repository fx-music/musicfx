package musicFx.basic;

import com.sun.javafx.scene.control.behavior.BehaviorBase;
import com.sun.javafx.scene.control.behavior.KeyBinding;
import javafx.scene.input.KeyCode;

import java.util.ArrayList;
import java.util.List;

public class FXCarouselBehavior extends BehaviorBase<FXCarousel> {
    protected static final List<KeyBinding> CAROUSEL_BINDINGS = new ArrayList<>();

    static {
        CAROUSEL_BINDINGS.add(new KeyBinding(KeyCode.LEFT, "previous"));
        CAROUSEL_BINDINGS.add(new KeyBinding(KeyCode.RIGHT, "next"));
    }

    public FXCarouselBehavior(final FXCarousel control) {
        super(control, CAROUSEL_BINDINGS);
    }

    @Override
    protected void callAction(String name) {
        FXCarousel carouselU = getControl();
        FXCarouselSkin skinU = (FXCarouselSkin) carouselU.getSkin();

        switch (name){
            case "previous" :
                skinU.previous();
                break;
            case "next" :
                skinU.next();
                break;
        }
    }
}
